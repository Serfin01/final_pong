/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    const int velocidady = 10;
    const int ballSize = 25;
    const int maxVelocity = 50;
    const int minVelocity = 10;
    
    char *texto;
    Vector2 tamanoTexto;
   
    bool pause = false;
   
    int score1p = 0;
    int score2p = 0;
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    int ballRadius;
    
    int playerLife;
    int enemyLife;
    
    int frameCount2 = 0;
    int timeCounter = 0;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 100;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
   
    Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
   
   
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    Vector2 ballVelocity;
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
   
   
    int iaLinex = screenWidth/2;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    int margin = 5;
    
    Rectangle backRect = { screenWidth/2 - 200, screenHeight/2 - 200, 400, 50};
    Rectangle fillRect = { backRect.x + margin, backRect.y + margin, backRect.width - (2 * margin), backRect.height - (2 * margin)};
    Rectangle lifeRect = fillRect;
    lifeRect.width /= 2;
    
    Rectangle backRect2 = { screenWidth - 200, 0 - 50, 400, 50};
    Rectangle fillRect2 = { backRect.x + margin, backRect.y + margin, backRect.width - (2 * margin), backRect.height - (2 * margin)};
    Rectangle lifeRect2 = fillRect;
    lifeRect2.width /= 2;
    
    Color lifeColor = YELLOW;
    
    
    
    int drainLife = 15;
    int healLife = 15;
    int winNum = 0;
    int loseNum = 0;
    
    bool win = false;
    bool lose = false;
    bool blink = true;
    int frameCount = 0;
    
    Vector2 point1, point2, point3;
    point1.x = screenWidth/2;
    point1.y = screenHeight/2;
   
    point2.x = screenWidth/2 - 60;
    point2.y = screenHeight/2 +50;
   
    point3.x = screenWidth/2 + 60;
    point3.y = screenHeight/2 +50;
    
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                if(fadeOut)
                    {
                        alpha += fadeSpeed;
                       
                        if(alpha >= 1.0f)
                        {
                            alpha = 1.0f;
                            fadeOut = !fadeOut;
                        }
                    }
                    else
                    {
                        alpha -= fadeSpeed;
                        if(alpha <= 0.0f)
                        {
                            alpha = 0.0f;
                            fadeOut = !fadeOut;
                        }
                    }
                if(alpha==1){
                    screen=TITLE;
                }
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
            frameCount++;
            if (frameCount%20  == 0)
            {
                blink = !blink;
                frameCount = 0;
            }
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if(IsKeyDown(KEY_ENTER)){
                    screen = GAMEPLAY;
                }
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                if(!pause){
                ball.x += ballVelocity.x;
                ball.y += ballVelocity.y;
                }
                // TODO: Player movement logic.......................(0.2p)
                if(!pause){
                    if (IsKeyDown(KEY_UP)){
                      paladerecha.y -= velocidady;
                    }
                   
                    if (IsKeyDown(KEY_DOWN)){
                      paladerecha.y += velocidady;
                    }
                    framesCounter++;

                    if (framesCounter % 60 == 0)
                    {
                        framesCounter = 0;
                        secondsCounter--;
                    }

                    if (secondsCounter < 0) screen = ENDING;
                }
                // TODO: Enemy movement logic (IA)...................(1p)
                if( ball.x < iaLinex){
                    if(ball.y > palaizquierda.y){
                        palaizquierda.y += velocidady;
                    }
                   
                    if(ball.y < paladerecha.y){
                        palaizquierda.y -= velocidady;
                    }
                }
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if(palaizquierda.y<0){
                    palaizquierda.y = 0;
                }
               
                if(palaizquierda.y > (screenHeight - palaizquierda.height)){
                    palaizquierda.y = screenHeight - palaizquierda.height;
                }
                
                if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){
                    if(ballVelocity.x<0){
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.5;
                            ballVelocity.y *= 1.5;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if(paladerecha.y<0){
                    paladerecha.y = 0;
                }
               
                if(paladerecha.y > (screenHeight - paladerecha.height)){
                    paladerecha.y = screenHeight - paladerecha.height;
                }
                
                if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){
                    if(ballVelocity.x>0){
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.5;
                            ballVelocity.y *= 1.5;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
                    ballVelocity.y *=-1;
                    //PlaySound(fxWav);
                }
                // TODO: Life bars decrease logic....................(1p)
                if (!win && !lose)
                    {
                        if (ball.x > screenWidth - ballSize) 
                        {
                            lifeRect.width += healLife;
                            ball.x = screenWidth/2;
                            ball.y = screenHeight/2;
                            ballVelocity.x = -minVelocity;
                            ballVelocity.y = minVelocity;
                        }else if(ball.x < ballSize) 
                        {
                            lifeRect.width -= drainLife;
                            lifeRect2.width -= drainLife;
                            ball.x = screenWidth/2;
                            ball.y = screenHeight/2;
                            ballVelocity.x = minVelocity;
                            ballVelocity.y = minVelocity;
                        }
                   
                        if (lifeRect.width <= 0)
                        {
                            win = true;
                            ++winNum;
                            lifeRect.width = 0;        
                        }
                       
                        else if (lifeRect.width >= fillRect.width)
                        {
                            lose = true;
                            ++loseNum;
                            lifeRect.width = fillRect.width;
                        }
                        if (lifeRect.width >= 2*(fillRect.width/3))
                        {
                            lifeColor = YELLOW;
                        }
                       
                        else if (lifeRect.width <= fillRect.width/3)
                        {
                            lifeColor = ORANGE;
                        }
                       
                        else
                        {
                            lifeColor = GREEN;
                        }
                    }
                    else if (winNum || loseNum == 3)
                    {
                        screen = ENDING;
                    }
                    else
                    {   
                        ballVelocity.x = 0;
                        ballVelocity.y = 0;
                        if (IsKeyPressed(KEY_R))
                        {
                            win = false;
                            lose = false;
                            lifeRect.width = fillRect.width/2;
                            ballVelocity.x = minVelocity;
                            ballVelocity.y = minVelocity;
                        }
                    }
                // TODO: Time counter logic..........................(0.2p)
                frameCount2++;
                if (frameCount2%60 == 0)
                {
                    timeCounter++;
                    frameCount2 = 0;
                }
                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                pause = !pause;
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    DrawTriangle(point1, point2, point3, Fade(RED, alpha));
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawText("Final Pong", (screenWidth - MeasureText("Final Pong", 60))/2, screenHeight/2 - (screenHeight/4), 60, BLACK);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText("PRESS ENTER!", (screenWidth - MeasureText("PRESS ENTER!", 60))/2, screenHeight/2 + (screenHeight/5), 60, BLACK);
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    ClearBackground(RAYWHITE);
                    if (win) DrawText("YOU WIN!", (screenWidth - MeasureText("YOU WIN!", 60))/2, screenHeight/2 - (screenHeight/4), 60, GREEN);
                    if (lose) DrawText("YOU LOSE...", (screenWidth - MeasureText("YOU LOSE...", 60))/2, screenHeight/2 - (screenHeight/4), 60, RED);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(paladerecha, GREEN);
                    DrawRectangleRec(palaizquierda, GREEN);
                    DrawCircleV(ball, ballSize, GREEN);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (backRect, BLACK);
                    DrawRectangleRec (fillRect, RED);
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect, lifeColor);
                    DrawRectangleRec (lifeRect2, lifeColor);
                    DrawText(FormatText("Wins: %i", winNum), screenWidth/8 - MeasureText(FormatText("Wins: %i", winNum), 40)/2, screenHeight/11, 40, BLACK);
                    DrawText(FormatText("Loses: %i", loseNum), 5*screenWidth/6 - MeasureText(FormatText("Loses: %i", loseNum), 40)/4, screenHeight/11, 40, BLACK);
                    if (win || lose) DrawText("PRESS R TO RETRY", (screenWidth - MeasureText("PRESS R TO RETRY", 40))/2, screenHeight/2 + (screenHeight/5), 40, PURPLE);
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2, 0, 40, GREEN);
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if(pause){
                    DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                    DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, RED);
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawText("Thanks for playing", (screenWidth - MeasureText("Thanks for playing", 60))/2, screenHeight/2 - (screenHeight/4), 60, BLACK);
                    DrawText(FormatText("%i - %i", winNum,loseNum), screenWidth/2 - MeasureText(FormatText("%i - %i", winNum,loseNum), 60)/2, screenHeight/2 + (screenHeight/10), 60, BLACK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}